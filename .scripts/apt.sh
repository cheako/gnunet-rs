#! /bin/bash

set -eux

echo >/etc/apt/sources.list.d/buster.list deb http://deb.debian.org/debian buster main
apt-get update -qq

APT_ARGS="-yqq --no-install-suggests --no-install-recommends --allow-change-held-packages"
apt-get install -t stable $APT_ARGS gettext autopoint iptables \
  libbluetooth-dev libcurl4-gnutls-dev libextractor-dev libgcrypt20-dev \
  libglpk-dev libgnutls28-dev libidn2-0-dev libjansson-dev \
  libogg-dev libopus-dev libpulse-dev libunistring-dev miniupnpc net-tools \
  python-dev python-future texinfo libclang-dev clang
apt-get install -t buster $APT_ARGS libmicrohttpd-dev
