#! /bin/bash

set -eux

addgroup gnunetdns
adduser --system --group --disabled-login --home /var/lib/gnunet gnunet

[ -f /var/tmp/gnunet/.git/config ] || {
	cd /var/tmp
	git clone --depth 1 --branch v0.11.3 https://gnunet.org/git/gnunet.git
}

cd /var/tmp/gnunet

#[ -f .dates.dat ] &&
#  perl -ne 'INIT{ $/ = "\0";} chomp; m!^([0-9]+)/([0-9]+)/([0-9]+)/(.*)!s or next; my ($ct, $mt, $at, $f) = ($1, $2, $3, $4); utime $at, $mt, $f;' .dates.dat

[ -f configure ] || sh ./bootstrap
[ -f Makefile ] || ./configure --prefix=/usr
[ -f src/core/.libs/gnunet-core ] || make
make install

#[ .dates.dat -nt src/core/.libs/gnunet-core ] ||
#  find . -not -path .dates.dat -print0 |
#  perl -ne 'INIT{ $/ = "\0"; use File::stat;} chomp; my $s = stat($_); next unless $s; print $s->ctime . "/" . $s->mtime . "/" . $s->atime ."/$_\0"; ' >.dates.dat
