#! /bin/bash

set -eux

git config --global user.email "${GIT_USER_EMAIL}"
git config --global user.name "gitlab pipelines"
git remote set-url origin https://${GITLAB_USERNAME}:${GITLAB_APP_PASSWORD}@gitlab.org/${GITLAB_REPO_OWNER}/${GITLAB_REPO_SLUG}.git
