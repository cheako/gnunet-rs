#! /bin/bash

set -eux

rustup component add rustfmt-preview
cargo fmt -v -- -v --emit files

if git commit -a -m "gitlab pipelines, rustfmt"; then
    git push
    echo "Pushed formatting changes. Another build will start."
    exit 1
fi
