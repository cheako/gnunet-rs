#! /bin/bash

set -eux

cargo doc --release --no-deps
if [ -f target/docwebsite/.git/config ]; then 
  (
    cd target/docwebsite
    sed -i .git/config -e "s/${CARGO_LIB_NAME}/${CARGO_LIB_NAME//-}/"
    git fetch --all
    git reset --hard "origin/$(git branch | awk '/^\*/ { print $2; exit 0; }')"
  )
else
  git clone https://${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}@bitbucket.org/${BITBUCKET_REPO_OWNER}/${CARGO_LIB_NAME//-}.bitbucket.io.git target/docwebsite
fi
rm -rvf target/docwebsite/*
mkdir target/docwebsite/include
cp -rv /usr/include/gnunet target/docwebsite/include
cp -rv target/doc/* target/docwebsite/
cp -v docwebsite/index.html target/docwebsite/
cd target/docwebsite
git add -A
git commit -m "bitbucket pipelines, ${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG} commit ${BITBUCKET_COMMIT}" || true
git push
