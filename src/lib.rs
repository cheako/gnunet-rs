pub mod ffi {
        #![allow(dead_code)]
        #![allow(non_upper_case_globals)]
        #![allow(non_camel_case_types)]
        #![allow(non_snake_case)]
        #![allow(improper_ctypes)]

        include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

use std::ffi::CString;
use std::os::raw::c_int;
use std::os::raw::c_void;

// Opaque handles
// grep '^pub struct GNUNET_[A-Z_]*_Handle' ./target/debug/build/gnunet-rs-16fec11cf8f98240/out/bindings.rs|grep -o 'GNUNET_[A-Z_]*_Handle '|tr \  ,
use ffi::{
        GNUNET_ARM_Handle, GNUNET_CONFIGURATION_Handle, GNUNET_DHT_Handle, GNUNET_GNS_Handle,
        GNUNET_HELPER_Handle, GNUNET_IDENTITY_Handle, GNUNET_MQ_Handle, GNUNET_NAMESTORE_Handle,
        GNUNET_NETWORK_Handle, GNUNET_OP_Handle, GNUNET_SCHEDULER_Handle, GNUNET_SERVICE_Handle,
};

pub struct ARMHandle {
        ptr: *mut GNUNET_ARM_Handle,
}

impl ARMHandle {
        pub fn request_service_start<F, T: Into<Vec<u8>>>(
                &self,
                t: T,
                mut closure: &mut F,
        ) -> *mut ffi::GNUNET_ARM_Operation
        where
                F: FnMut(ffi::GNUNET_ARM_RequestStatus, ffi::GNUNET_ARM_Result),
        {
                unsafe {
                        let (callback, closure) = unpack_arm_result(&mut closure);
                        ffi::GNUNET_ARM_request_service_start(
                                self.ptr,
                                CString::new(t).unwrap().as_ptr(),
                                0,
                                callback,
                                closure,
                        )
                }
        }
}

unsafe fn unpack_arm_result<F>(closure: &mut F) -> (ffi::GNUNET_ARM_ResultCallback, *mut c_void)
where
        F: FnMut(ffi::GNUNET_ARM_RequestStatus, ffi::GNUNET_ARM_Result),
{
        extern "C" fn trampoline<F>(
                data: *mut c_void,
                a: ffi::GNUNET_ARM_RequestStatus,
                b: ffi::GNUNET_ARM_Result,
        ) where
                F: FnMut(ffi::GNUNET_ARM_RequestStatus, ffi::GNUNET_ARM_Result),
        {
                let closure: &mut F = unsafe { &mut *(data as *mut F) };
                (*closure)(a, b)
        }

        (Some(trampoline::<F>), closure as *mut F as *mut c_void)
}

pub struct ConfigurationHandle {
        ptr: *mut GNUNET_CONFIGURATION_Handle,
}

pub fn configuration_create() -> ConfigurationHandle {
        ConfigurationHandle {
                ptr: unsafe { ffi::GNUNET_CONFIGURATION_create() },
        }
}

impl Clone for ConfigurationHandle {
        fn clone(&self) -> ConfigurationHandle {
                ConfigurationHandle {
                        ptr: unsafe { ffi::GNUNET_CONFIGURATION_dup(self.ptr) },
                }
        }
}

impl ConfigurationHandle {
        pub fn arm_connect<F>(&self, mut closure: &mut F) -> ARMHandle
        where
                F: FnMut(i32),
        {
                unsafe {
                        let (callback, closure) = unpack_arm_connection_status(&mut closure);
                        ARMHandle {
                                ptr: ffi::GNUNET_ARM_connect(self.ptr, callback, closure),
                        }
                }
        }
}

impl Drop for ConfigurationHandle {
        fn drop(&mut self) {
                unsafe {
                        ffi::GNUNET_CONFIGURATION_destroy(self.ptr);
                };
        }
}

unsafe fn unpack_arm_connection_status<F>(
        closure: &mut F,
) -> (ffi::GNUNET_ARM_ConnectionStatusCallback, *mut c_void)
where
        F: FnMut(i32),
{
        extern "C" fn trampoline<F>(data: *mut c_void, n: c_int)
        where
                F: FnMut(i32),
        {
                let closure: &mut F = unsafe { &mut *(data as *mut F) };
                (*closure)(n as i32)
        }

        (Some(trampoline::<F>), closure as *mut F as *mut c_void)
}

#[cfg(test)]
mod tests {
        #[test]
        fn it_works() {
                assert_eq!(2 + 2, 4);
        }
}

// Const numbers
// grep '^pub const GNUNET_' ./target/debug/build/gnunet-rs-16fec11cf8f98240/out/bindings.rs|grep -v usize|grep -o ' GNUNET_[A-Z_]*:'|tr : ,
pub use ffi::{
        GNUNET_ARM_VERSION, GNUNET_CRYPTO_AES_KEY_LENGTH,
        GNUNET_CRYPTO_ECC_SIGNATURE_DATA_ENCODING_LENGTH, GNUNET_CRYPTO_HASH_LENGTH,
        GNUNET_CRYPTO_PAILLIER_BITS, GNUNET_CRYPTO_PKEY_ASCII_LENGTH,
        GNUNET_DNSPARSER_MAX_LABEL_LENGTH, GNUNET_DNSPARSER_MAX_NAME_LENGTH,
        GNUNET_DNSPARSER_TYPE_A, GNUNET_DNSPARSER_TYPE_AAAA, GNUNET_DNSPARSER_TYPE_AFSDB,
        GNUNET_DNSPARSER_TYPE_ALL, GNUNET_DNSPARSER_TYPE_ANY, GNUNET_DNSPARSER_TYPE_APL,
        GNUNET_DNSPARSER_TYPE_CDNSKEY, GNUNET_DNSPARSER_TYPE_CDS, GNUNET_DNSPARSER_TYPE_CERT,
        GNUNET_DNSPARSER_TYPE_CNAME, GNUNET_DNSPARSER_TYPE_DHCID, GNUNET_DNSPARSER_TYPE_DNAME,
        GNUNET_DNSPARSER_TYPE_DNSKEY, GNUNET_DNSPARSER_TYPE_DS, GNUNET_DNSPARSER_TYPE_HIP,
        GNUNET_DNSPARSER_TYPE_IPSECKEY, GNUNET_DNSPARSER_TYPE_KEY, GNUNET_DNSPARSER_TYPE_KX,
        GNUNET_DNSPARSER_TYPE_LOC, GNUNET_DNSPARSER_TYPE_MX, GNUNET_DNSPARSER_TYPE_NAPTR,
        GNUNET_DNSPARSER_TYPE_NS, GNUNET_DNSPARSER_TYPE_NSEC, GNUNET_DNSPARSER_TYPE_OPENPGPKEY,
        GNUNET_DNSPARSER_TYPE_PTR, GNUNET_DNSPARSER_TYPE_RP, GNUNET_DNSPARSER_TYPE_RRSIG,
        GNUNET_DNSPARSER_TYPE_SIG, GNUNET_DNSPARSER_TYPE_SOA, GNUNET_DNSPARSER_TYPE_SRV,
        GNUNET_DNSPARSER_TYPE_SSHFP, GNUNET_DNSPARSER_TYPE_TA, GNUNET_DNSPARSER_TYPE_TKEY,
        GNUNET_DNSPARSER_TYPE_TLSA, GNUNET_DNSPARSER_TYPE_TSIG, GNUNET_DNSPARSER_TYPE_TXT,
        GNUNET_DNSPARSER_TYPE_URI, GNUNET_EXTRA_LOGGING, GNUNET_GNSRECORD_MAX_BLOCK_SIZE,
        GNUNET_GNSRECORD_TYPE_ABE_KEY, GNUNET_GNSRECORD_TYPE_ABE_MASTER, GNUNET_GNSRECORD_TYPE_ANY,
        GNUNET_GNSRECORD_TYPE_ATTRIBUTE, GNUNET_GNSRECORD_TYPE_BOX,
        GNUNET_GNSRECORD_TYPE_CREDENTIAL, GNUNET_GNSRECORD_TYPE_ID_ATTR,
        GNUNET_GNSRECORD_TYPE_ID_TOKEN, GNUNET_GNSRECORD_TYPE_ID_TOKEN_METADATA,
        GNUNET_GNSRECORD_TYPE_LEHO, GNUNET_GNSRECORD_TYPE_NICK, GNUNET_GNSRECORD_TYPE_PHONE,
        GNUNET_GNSRECORD_TYPE_PKEY, GNUNET_GNSRECORD_TYPE_PLACE, GNUNET_GNSRECORD_TYPE_POLICY,
        GNUNET_GNSRECORD_TYPE_RECLAIM_OIDC_CLIENT, GNUNET_GNSRECORD_TYPE_RECLAIM_OIDC_REDIRECT,
        GNUNET_GNSRECORD_TYPE_VPN, GNUNET_HELLO_URI_SEP, GNUNET_IDENTITY_VERSION,
        GNUNET_LOG_CALL_STATUS, GNUNET_MAX_MALLOC_CHECKED, GNUNET_MAX_MESSAGE_SIZE,
        GNUNET_MESSAGE_TYPE_ALL, GNUNET_MESSAGE_TYPE_ARM_LIST, GNUNET_MESSAGE_TYPE_ARM_LIST_RESULT,
        GNUNET_MESSAGE_TYPE_ARM_MONITOR, GNUNET_MESSAGE_TYPE_ARM_RESULT,
        GNUNET_MESSAGE_TYPE_ARM_START, GNUNET_MESSAGE_TYPE_ARM_STATUS,
        GNUNET_MESSAGE_TYPE_ARM_STOP, GNUNET_MESSAGE_TYPE_ARM_TEST,
        GNUNET_MESSAGE_TYPE_ATS_ADDRESSLIST_REQUEST, GNUNET_MESSAGE_TYPE_ATS_ADDRESSLIST_RESPONSE,
        GNUNET_MESSAGE_TYPE_ATS_ADDRESS_ADD, GNUNET_MESSAGE_TYPE_ATS_ADDRESS_DESTROYED,
        GNUNET_MESSAGE_TYPE_ATS_ADDRESS_SUGGESTION, GNUNET_MESSAGE_TYPE_ATS_ADDRESS_UPDATE,
        GNUNET_MESSAGE_TYPE_ATS_PEER_INFORMATION, GNUNET_MESSAGE_TYPE_ATS_PREFERENCE_CHANGE,
        GNUNET_MESSAGE_TYPE_ATS_PREFERENCE_FEEDBACK, GNUNET_MESSAGE_TYPE_ATS_REQUEST_ADDRESS,
        GNUNET_MESSAGE_TYPE_ATS_REQUEST_ADDRESS_CANCEL,
        GNUNET_MESSAGE_TYPE_ATS_RESERVATION_REQUEST, GNUNET_MESSAGE_TYPE_ATS_RESERVATION_RESULT,
        GNUNET_MESSAGE_TYPE_ATS_SESSION_ADD, GNUNET_MESSAGE_TYPE_ATS_SESSION_ADD_INBOUND_ONLY,
        GNUNET_MESSAGE_TYPE_ATS_SESSION_ALLOCATION, GNUNET_MESSAGE_TYPE_ATS_SESSION_DEL,
        GNUNET_MESSAGE_TYPE_ATS_SESSION_RELEASE, GNUNET_MESSAGE_TYPE_ATS_SESSION_UPDATE,
        GNUNET_MESSAGE_TYPE_ATS_START, GNUNET_MESSAGE_TYPE_ATS_SUGGEST,
        GNUNET_MESSAGE_TYPE_ATS_SUGGEST_CANCEL, GNUNET_MESSAGE_TYPE_AUCTION_CLIENT_CREATE,
        GNUNET_MESSAGE_TYPE_AUCTION_CLIENT_JOIN, GNUNET_MESSAGE_TYPE_AUCTION_CLIENT_OUTCOME,
        GNUNET_MESSAGE_TYPE_CADET_CHANNEL_APP_DATA, GNUNET_MESSAGE_TYPE_CADET_CHANNEL_APP_DATA_ACK,
        GNUNET_MESSAGE_TYPE_CADET_CHANNEL_DESTROY, GNUNET_MESSAGE_TYPE_CADET_CHANNEL_KEEPALIVE,
        GNUNET_MESSAGE_TYPE_CADET_CHANNEL_OPEN, GNUNET_MESSAGE_TYPE_CADET_CHANNEL_OPEN_ACK,
        GNUNET_MESSAGE_TYPE_CADET_CHANNEL_OPEN_NACK_DEPRECATED, GNUNET_MESSAGE_TYPE_CADET_CLI,
        GNUNET_MESSAGE_TYPE_CADET_CONNECTION_BROKEN, GNUNET_MESSAGE_TYPE_CADET_CONNECTION_CREATE,
        GNUNET_MESSAGE_TYPE_CADET_CONNECTION_CREATE_ACK,
        GNUNET_MESSAGE_TYPE_CADET_CONNECTION_DESTROY,
        GNUNET_MESSAGE_TYPE_CADET_CONNECTION_HOP_BY_HOP_ENCRYPTED_ACK,
        GNUNET_MESSAGE_TYPE_CADET_CONNECTION_PATH_CHANGED_UNIMPLEMENTED,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_ACK, GNUNET_MESSAGE_TYPE_CADET_LOCAL_CHANNEL_CREATE,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_CHANNEL_DESTROY, GNUNET_MESSAGE_TYPE_CADET_LOCAL_DATA,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_INFO_CHANNEL,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_INFO_CHANNEL_END,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_INFO_PATH, GNUNET_MESSAGE_TYPE_CADET_LOCAL_INFO_PATH_END,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_INFO_PEERS, GNUNET_MESSAGE_TYPE_CADET_LOCAL_INFO_PEERS_END,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_INFO_TUNNELS,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_INFO_TUNNELS_END,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_PORT_CLOSE, GNUNET_MESSAGE_TYPE_CADET_LOCAL_PORT_OPEN,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_REQUEST_INFO_CHANNEL,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_REQUEST_INFO_PATH,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_REQUEST_INFO_PEERS,
        GNUNET_MESSAGE_TYPE_CADET_LOCAL_REQUEST_INFO_TUNNELS,
        GNUNET_MESSAGE_TYPE_CADET_TUNNEL_ENCRYPTED,
        GNUNET_MESSAGE_TYPE_CADET_TUNNEL_ENCRYPTED_POLL, GNUNET_MESSAGE_TYPE_CADET_TUNNEL_KX,
        GNUNET_MESSAGE_TYPE_CADET_TUNNEL_KX_AUTH,
        GNUNET_MESSAGE_TYPE_CHAT_CONFIRMATION_NOTIFICATION,
        GNUNET_MESSAGE_TYPE_CHAT_CONFIRMATION_RECEIPT, GNUNET_MESSAGE_TYPE_CHAT_JOIN_NOTIFICATION,
        GNUNET_MESSAGE_TYPE_CHAT_JOIN_REQUEST, GNUNET_MESSAGE_TYPE_CHAT_LEAVE_NOTIFICATION,
        GNUNET_MESSAGE_TYPE_CHAT_MESSAGE_NOTIFICATION, GNUNET_MESSAGE_TYPE_CHAT_TRANSMIT_REQUEST,
        GNUNET_MESSAGE_TYPE_COMMUNICATOR_TCP_BOX, GNUNET_MESSAGE_TYPE_COMMUNICATOR_TCP_FINISH,
        GNUNET_MESSAGE_TYPE_COMMUNICATOR_TCP_REKEY, GNUNET_MESSAGE_TYPE_COMMUNICATOR_UDP_ACK,
        GNUNET_MESSAGE_TYPE_COMMUNICATOR_UDP_PAD, GNUNET_MESSAGE_TYPE_CONSENSUS_CLIENT_ACK,
        GNUNET_MESSAGE_TYPE_CONSENSUS_CLIENT_BEGIN, GNUNET_MESSAGE_TYPE_CONSENSUS_CLIENT_CONCLUDE,
        GNUNET_MESSAGE_TYPE_CONSENSUS_CLIENT_CONCLUDE_DONE,
        GNUNET_MESSAGE_TYPE_CONSENSUS_CLIENT_INSERT, GNUNET_MESSAGE_TYPE_CONSENSUS_CLIENT_JOIN,
        GNUNET_MESSAGE_TYPE_CONSENSUS_CLIENT_RECEIVED_ELEMENT,
        GNUNET_MESSAGE_TYPE_CONVERSATION_AUDIO, GNUNET_MESSAGE_TYPE_CONVERSATION_CADET_AUDIO,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CADET_PHONE_HANG_UP,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CADET_PHONE_PICK_UP,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CADET_PHONE_RESUME,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CADET_PHONE_RING,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CADET_PHONE_SUSPEND,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CS_AUDIO, GNUNET_MESSAGE_TYPE_CONVERSATION_CS_PHONE_CALL,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CS_PHONE_HANG_UP,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CS_PHONE_PICKED_UP,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CS_PHONE_PICK_UP,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CS_PHONE_REGISTER,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CS_PHONE_RESUME,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CS_PHONE_RING,
        GNUNET_MESSAGE_TYPE_CONVERSATION_CS_PHONE_SUSPEND,
        GNUNET_MESSAGE_TYPE_CORE_BINARY_TYPE_MAP, GNUNET_MESSAGE_TYPE_CORE_COMPRESSED_TYPE_MAP,
        GNUNET_MESSAGE_TYPE_CORE_CONFIRM_TYPE_MAP, GNUNET_MESSAGE_TYPE_CORE_ENCRYPTED_MESSAGE,
        GNUNET_MESSAGE_TYPE_CORE_EPHEMERAL_KEY, GNUNET_MESSAGE_TYPE_CORE_HANGUP,
        GNUNET_MESSAGE_TYPE_CORE_INIT, GNUNET_MESSAGE_TYPE_CORE_INIT_REPLY,
        GNUNET_MESSAGE_TYPE_CORE_MONITOR_NOTIFY, GNUNET_MESSAGE_TYPE_CORE_MONITOR_PEERS,
        GNUNET_MESSAGE_TYPE_CORE_NOTIFY_CONNECT, GNUNET_MESSAGE_TYPE_CORE_NOTIFY_DISCONNECT,
        GNUNET_MESSAGE_TYPE_CORE_NOTIFY_INBOUND, GNUNET_MESSAGE_TYPE_CORE_NOTIFY_OUTBOUND,
        GNUNET_MESSAGE_TYPE_CORE_NOTIFY_STATUS_CHANGE, GNUNET_MESSAGE_TYPE_CORE_PING,
        GNUNET_MESSAGE_TYPE_CORE_PONG, GNUNET_MESSAGE_TYPE_CORE_SEND,
        GNUNET_MESSAGE_TYPE_CORE_SEND_READY, GNUNET_MESSAGE_TYPE_CORE_SEND_REQUEST,
        GNUNET_MESSAGE_TYPE_CREDENTIAL_COLLECT, GNUNET_MESSAGE_TYPE_CREDENTIAL_COLLECT_RESULT,
        GNUNET_MESSAGE_TYPE_CREDENTIAL_VERIFY, GNUNET_MESSAGE_TYPE_CREDENTIAL_VERIFY_RESULT,
        GNUNET_MESSAGE_TYPE_DATASTORE_DATA, GNUNET_MESSAGE_TYPE_DATASTORE_DATA_END,
        GNUNET_MESSAGE_TYPE_DATASTORE_DROP, GNUNET_MESSAGE_TYPE_DATASTORE_GET,
        GNUNET_MESSAGE_TYPE_DATASTORE_GET_KEY, GNUNET_MESSAGE_TYPE_DATASTORE_GET_REPLICATION,
        GNUNET_MESSAGE_TYPE_DATASTORE_GET_ZERO_ANONYMITY, GNUNET_MESSAGE_TYPE_DATASTORE_PUT,
        GNUNET_MESSAGE_TYPE_DATASTORE_RELEASE_RESERVE, GNUNET_MESSAGE_TYPE_DATASTORE_REMOVE,
        GNUNET_MESSAGE_TYPE_DATASTORE_RESERVE, GNUNET_MESSAGE_TYPE_DATASTORE_STATUS,
        GNUNET_MESSAGE_TYPE_DHT_CLIENT_GET, GNUNET_MESSAGE_TYPE_DHT_CLIENT_GET_RESULTS_KNOWN,
        GNUNET_MESSAGE_TYPE_DHT_CLIENT_GET_STOP, GNUNET_MESSAGE_TYPE_DHT_CLIENT_PUT,
        GNUNET_MESSAGE_TYPE_DHT_CLIENT_RESULT, GNUNET_MESSAGE_TYPE_DHT_MONITOR_GET,
        GNUNET_MESSAGE_TYPE_DHT_MONITOR_GET_RESP, GNUNET_MESSAGE_TYPE_DHT_MONITOR_PUT,
        GNUNET_MESSAGE_TYPE_DHT_MONITOR_PUT_RESP, GNUNET_MESSAGE_TYPE_DHT_MONITOR_START,
        GNUNET_MESSAGE_TYPE_DHT_MONITOR_STOP, GNUNET_MESSAGE_TYPE_DNS_CLIENT_INIT,
        GNUNET_MESSAGE_TYPE_DNS_CLIENT_REQUEST, GNUNET_MESSAGE_TYPE_DNS_CLIENT_RESPONSE,
        GNUNET_MESSAGE_TYPE_DNS_HELPER, GNUNET_MESSAGE_TYPE_DUMMY, GNUNET_MESSAGE_TYPE_DV_BOX,
        GNUNET_MESSAGE_TYPE_DV_CONNECT, GNUNET_MESSAGE_TYPE_DV_DISCONNECT,
        GNUNET_MESSAGE_TYPE_DV_DISTANCE_CHANGED, GNUNET_MESSAGE_TYPE_DV_RECV,
        GNUNET_MESSAGE_TYPE_DV_ROUTE, GNUNET_MESSAGE_TYPE_DV_SEND, GNUNET_MESSAGE_TYPE_DV_SEND_ACK,
        GNUNET_MESSAGE_TYPE_DV_SEND_NACK, GNUNET_MESSAGE_TYPE_DV_START,
        GNUNET_MESSAGE_TYPE_FRAGMENT, GNUNET_MESSAGE_TYPE_FRAGMENT_ACK,
        GNUNET_MESSAGE_TYPE_FS_CADET_QUERY, GNUNET_MESSAGE_TYPE_FS_CADET_REPLY,
        GNUNET_MESSAGE_TYPE_FS_GET, GNUNET_MESSAGE_TYPE_FS_INDEX_LIST_END,
        GNUNET_MESSAGE_TYPE_FS_INDEX_LIST_ENTRY, GNUNET_MESSAGE_TYPE_FS_INDEX_LIST_GET,
        GNUNET_MESSAGE_TYPE_FS_INDEX_START, GNUNET_MESSAGE_TYPE_FS_INDEX_START_FAILED,
        GNUNET_MESSAGE_TYPE_FS_INDEX_START_OK, GNUNET_MESSAGE_TYPE_FS_MIGRATION_STOP,
        GNUNET_MESSAGE_TYPE_FS_PUBLISH_HELPER_COUNTING_DONE,
        GNUNET_MESSAGE_TYPE_FS_PUBLISH_HELPER_ERROR,
        GNUNET_MESSAGE_TYPE_FS_PUBLISH_HELPER_FINISHED,
        GNUNET_MESSAGE_TYPE_FS_PUBLISH_HELPER_META_DATA,
        GNUNET_MESSAGE_TYPE_FS_PUBLISH_HELPER_PROGRESS_DIRECTORY,
        GNUNET_MESSAGE_TYPE_FS_PUBLISH_HELPER_PROGRESS_FILE,
        GNUNET_MESSAGE_TYPE_FS_PUBLISH_HELPER_SKIP_FILE, GNUNET_MESSAGE_TYPE_FS_PUT,
        GNUNET_MESSAGE_TYPE_FS_REQUEST_LOC_SIGN, GNUNET_MESSAGE_TYPE_FS_REQUEST_LOC_SIGNATURE,
        GNUNET_MESSAGE_TYPE_FS_START_SEARCH, GNUNET_MESSAGE_TYPE_FS_UNINDEX,
        GNUNET_MESSAGE_TYPE_FS_UNINDEX_OK, GNUNET_MESSAGE_TYPE_GNS_LOOKUP,
        GNUNET_MESSAGE_TYPE_GNS_LOOKUP_RESULT, GNUNET_MESSAGE_TYPE_GNS_REVERSE_LOOKUP,
        GNUNET_MESSAGE_TYPE_GNS_REVERSE_LOOKUP_RESULT, GNUNET_MESSAGE_TYPE_HELLO,
        GNUNET_MESSAGE_TYPE_HELLO_LEGACY, GNUNET_MESSAGE_TYPE_HOSTLIST_ADVERTISEMENT,
        GNUNET_MESSAGE_TYPE_IDENTITY_CREATE, GNUNET_MESSAGE_TYPE_IDENTITY_DELETE,
        GNUNET_MESSAGE_TYPE_IDENTITY_GET_DEFAULT, GNUNET_MESSAGE_TYPE_IDENTITY_RENAME,
        GNUNET_MESSAGE_TYPE_IDENTITY_RESULT_CODE, GNUNET_MESSAGE_TYPE_IDENTITY_SET_DEFAULT,
        GNUNET_MESSAGE_TYPE_IDENTITY_START, GNUNET_MESSAGE_TYPE_IDENTITY_UPDATE,
        GNUNET_MESSAGE_TYPE_LOCKMANAGER_ACQUIRE, GNUNET_MESSAGE_TYPE_LOCKMANAGER_RELEASE,
        GNUNET_MESSAGE_TYPE_LOCKMANAGER_SUCCESS, GNUNET_MESSAGE_TYPE_MULTICAST_FRAGMENT_ACK,
        GNUNET_MESSAGE_TYPE_MULTICAST_GROUP_END, GNUNET_MESSAGE_TYPE_MULTICAST_JOIN_DECISION,
        GNUNET_MESSAGE_TYPE_MULTICAST_JOIN_REQUEST, GNUNET_MESSAGE_TYPE_MULTICAST_MEMBER_JOIN,
        GNUNET_MESSAGE_TYPE_MULTICAST_MESSAGE, GNUNET_MESSAGE_TYPE_MULTICAST_ORIGIN_START,
        GNUNET_MESSAGE_TYPE_MULTICAST_PART_ACK, GNUNET_MESSAGE_TYPE_MULTICAST_PART_REQUEST,
        GNUNET_MESSAGE_TYPE_MULTICAST_REPLAY_REQUEST,
        GNUNET_MESSAGE_TYPE_MULTICAST_REPLAY_RESPONSE,
        GNUNET_MESSAGE_TYPE_MULTICAST_REPLAY_RESPONSE_END, GNUNET_MESSAGE_TYPE_MULTICAST_REQUEST,
        GNUNET_MESSAGE_TYPE_NAMECACHE_BLOCK_CACHE,
        GNUNET_MESSAGE_TYPE_NAMECACHE_BLOCK_CACHE_RESPONSE,
        GNUNET_MESSAGE_TYPE_NAMECACHE_LOOKUP_BLOCK,
        GNUNET_MESSAGE_TYPE_NAMECACHE_LOOKUP_BLOCK_RESPONSE,
        GNUNET_MESSAGE_TYPE_NAMESTORE_MONITOR_NEXT, GNUNET_MESSAGE_TYPE_NAMESTORE_MONITOR_START,
        GNUNET_MESSAGE_TYPE_NAMESTORE_MONITOR_SYNC, GNUNET_MESSAGE_TYPE_NAMESTORE_RECORD_LOOKUP,
        GNUNET_MESSAGE_TYPE_NAMESTORE_RECORD_LOOKUP_RESPONSE,
        GNUNET_MESSAGE_TYPE_NAMESTORE_RECORD_RESULT,
        GNUNET_MESSAGE_TYPE_NAMESTORE_RECORD_RESULT_END,
        GNUNET_MESSAGE_TYPE_NAMESTORE_RECORD_STORE,
        GNUNET_MESSAGE_TYPE_NAMESTORE_RECORD_STORE_RESPONSE,
        GNUNET_MESSAGE_TYPE_NAMESTORE_ZONE_ITERATION_NEXT,
        GNUNET_MESSAGE_TYPE_NAMESTORE_ZONE_ITERATION_START,
        GNUNET_MESSAGE_TYPE_NAMESTORE_ZONE_ITERATION_STOP,
        GNUNET_MESSAGE_TYPE_NAMESTORE_ZONE_TO_NAME,
        GNUNET_MESSAGE_TYPE_NAMESTORE_ZONE_TO_NAME_RESPONSE,
        GNUNET_MESSAGE_TYPE_NAT_ADDRESS_CHANGE, GNUNET_MESSAGE_TYPE_NAT_AUTO_CFG_RESULT,
        GNUNET_MESSAGE_TYPE_NAT_AUTO_REQUEST_CFG,
        GNUNET_MESSAGE_TYPE_NAT_CONNECTION_REVERSAL_REQUESTED, GNUNET_MESSAGE_TYPE_NAT_HANDLE_STUN,
        GNUNET_MESSAGE_TYPE_NAT_REGISTER, GNUNET_MESSAGE_TYPE_NAT_REQUEST_CONNECTION_REVERSAL,
        GNUNET_MESSAGE_TYPE_NAT_TEST, GNUNET_MESSAGE_TYPE_NSE_ESTIMATE,
        GNUNET_MESSAGE_TYPE_NSE_START, GNUNET_MESSAGE_TYPE_PEERINFO_GET,
        GNUNET_MESSAGE_TYPE_PEERINFO_GET_ALL, GNUNET_MESSAGE_TYPE_PEERINFO_INFO,
        GNUNET_MESSAGE_TYPE_PEERINFO_INFO_END, GNUNET_MESSAGE_TYPE_PEERINFO_NOTIFY,
        GNUNET_MESSAGE_TYPE_PEERSTORE_ITERATE, GNUNET_MESSAGE_TYPE_PEERSTORE_ITERATE_END,
        GNUNET_MESSAGE_TYPE_PEERSTORE_ITERATE_RECORD, GNUNET_MESSAGE_TYPE_PEERSTORE_STORE,
        GNUNET_MESSAGE_TYPE_PEERSTORE_WATCH, GNUNET_MESSAGE_TYPE_PEERSTORE_WATCH_CANCEL,
        GNUNET_MESSAGE_TYPE_PEERSTORE_WATCH_RECORD, GNUNET_MESSAGE_TYPE_PSYCSTORE_COUNTERS_GET,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_FRAGMENT_GET, GNUNET_MESSAGE_TYPE_PSYCSTORE_FRAGMENT_STORE,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_MEMBERSHIP_STORE,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_MEMBERSHIP_TEST, GNUNET_MESSAGE_TYPE_PSYCSTORE_MESSAGE_GET,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_MESSAGE_GET_FRAGMENT,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_RESULT_CODE, GNUNET_MESSAGE_TYPE_PSYCSTORE_RESULT_COUNTERS,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_RESULT_FRAGMENT, GNUNET_MESSAGE_TYPE_PSYCSTORE_RESULT_STATE,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_STATE_GET, GNUNET_MESSAGE_TYPE_PSYCSTORE_STATE_GET_PREFIX,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_STATE_HASH_UPDATE,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_STATE_MODIFY, GNUNET_MESSAGE_TYPE_PSYCSTORE_STATE_RESET,
        GNUNET_MESSAGE_TYPE_PSYCSTORE_STATE_SYNC,
        GNUNET_MESSAGE_TYPE_PSYC_CHANNEL_MEMBERSHIP_STORE, GNUNET_MESSAGE_TYPE_PSYC_HISTORY_REPLAY,
        GNUNET_MESSAGE_TYPE_PSYC_HISTORY_RESULT, GNUNET_MESSAGE_TYPE_PSYC_JOIN_DECISION,
        GNUNET_MESSAGE_TYPE_PSYC_JOIN_REQUEST, GNUNET_MESSAGE_TYPE_PSYC_MASTER_START,
        GNUNET_MESSAGE_TYPE_PSYC_MASTER_START_ACK, GNUNET_MESSAGE_TYPE_PSYC_MESSAGE,
        GNUNET_MESSAGE_TYPE_PSYC_MESSAGE_ACK, GNUNET_MESSAGE_TYPE_PSYC_MESSAGE_CANCEL,
        GNUNET_MESSAGE_TYPE_PSYC_MESSAGE_DATA, GNUNET_MESSAGE_TYPE_PSYC_MESSAGE_END,
        GNUNET_MESSAGE_TYPE_PSYC_MESSAGE_HEADER, GNUNET_MESSAGE_TYPE_PSYC_MESSAGE_METHOD,
        GNUNET_MESSAGE_TYPE_PSYC_MESSAGE_MODIFIER, GNUNET_MESSAGE_TYPE_PSYC_MESSAGE_MOD_CONT,
        GNUNET_MESSAGE_TYPE_PSYC_PART_ACK, GNUNET_MESSAGE_TYPE_PSYC_PART_REQUEST,
        GNUNET_MESSAGE_TYPE_PSYC_RESULT_CODE, GNUNET_MESSAGE_TYPE_PSYC_SLAVE_JOIN,
        GNUNET_MESSAGE_TYPE_PSYC_SLAVE_JOIN_ACK, GNUNET_MESSAGE_TYPE_PSYC_STATE_GET,
        GNUNET_MESSAGE_TYPE_PSYC_STATE_GET_PREFIX, GNUNET_MESSAGE_TYPE_PSYC_STATE_RESULT,
        GNUNET_MESSAGE_TYPE_RECLAIM_ATTRIBUTE_ITERATION_NEXT,
        GNUNET_MESSAGE_TYPE_RECLAIM_ATTRIBUTE_ITERATION_START,
        GNUNET_MESSAGE_TYPE_RECLAIM_ATTRIBUTE_ITERATION_STOP,
        GNUNET_MESSAGE_TYPE_RECLAIM_ATTRIBUTE_RESULT, GNUNET_MESSAGE_TYPE_RECLAIM_ATTRIBUTE_STORE,
        GNUNET_MESSAGE_TYPE_RECLAIM_ATTRIBUTE_STORE_RESPONSE,
        GNUNET_MESSAGE_TYPE_RECLAIM_CONSUME_TICKET,
        GNUNET_MESSAGE_TYPE_RECLAIM_CONSUME_TICKET_RESULT,
        GNUNET_MESSAGE_TYPE_RECLAIM_ISSUE_TICKET, GNUNET_MESSAGE_TYPE_RECLAIM_REVOKE_TICKET,
        GNUNET_MESSAGE_TYPE_RECLAIM_REVOKE_TICKET_RESULT,
        GNUNET_MESSAGE_TYPE_RECLAIM_TICKET_ITERATION_NEXT,
        GNUNET_MESSAGE_TYPE_RECLAIM_TICKET_ITERATION_START,
        GNUNET_MESSAGE_TYPE_RECLAIM_TICKET_ITERATION_STOP,
        GNUNET_MESSAGE_TYPE_RECLAIM_TICKET_RESULT, GNUNET_MESSAGE_TYPE_REGEX_ANNOUNCE,
        GNUNET_MESSAGE_TYPE_REGEX_RESULT, GNUNET_MESSAGE_TYPE_REGEX_SEARCH,
        GNUNET_MESSAGE_TYPE_REQUEST_AGPL, GNUNET_MESSAGE_TYPE_RESOLVER_REQUEST,
        GNUNET_MESSAGE_TYPE_RESOLVER_RESPONSE, GNUNET_MESSAGE_TYPE_RESPONSE_AGPL,
        GNUNET_MESSAGE_TYPE_REVOCATION_QUERY, GNUNET_MESSAGE_TYPE_REVOCATION_QUERY_RESPONSE,
        GNUNET_MESSAGE_TYPE_REVOCATION_REVOKE, GNUNET_MESSAGE_TYPE_REVOCATION_REVOKE_RESPONSE,
        GNUNET_MESSAGE_TYPE_RPS_CS_DEBUG_STREAM_CANCEL,
        GNUNET_MESSAGE_TYPE_RPS_CS_DEBUG_STREAM_REPLY,
        GNUNET_MESSAGE_TYPE_RPS_CS_DEBUG_STREAM_REQUEST,
        GNUNET_MESSAGE_TYPE_RPS_CS_DEBUG_VIEW_CANCEL, GNUNET_MESSAGE_TYPE_RPS_CS_DEBUG_VIEW_REPLY,
        GNUNET_MESSAGE_TYPE_RPS_CS_DEBUG_VIEW_REQUEST, GNUNET_MESSAGE_TYPE_RPS_CS_SEED,
        GNUNET_MESSAGE_TYPE_RPS_CS_SUB_START, GNUNET_MESSAGE_TYPE_RPS_CS_SUB_STOP,
        GNUNET_MESSAGE_TYPE_RPS_PP_CHECK_LIVE, GNUNET_MESSAGE_TYPE_RPS_PP_PULL_REPLY,
        GNUNET_MESSAGE_TYPE_RPS_PP_PULL_REQUEST, GNUNET_MESSAGE_TYPE_RPS_PP_PUSH,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_ALICE_CRYPTODATA,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_BOB_CRYPTODATA,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_BOB_CRYPTODATA_MULTIPART,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_CLIENT_MULTIPART_ALICE,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_CLIENT_MULTIPART_BOB,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_CLIENT_TO_ALICE,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_CLIENT_TO_BOB,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_ECC_ALICE_CRYPTODATA,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_ECC_BOB_CRYPTODATA,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_ECC_SESSION_INITIALIZATION,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_RESULT,
        GNUNET_MESSAGE_TYPE_SCALARPRODUCT_SESSION_INITIALIZATION,
        GNUNET_MESSAGE_TYPE_SECRETSHARING_CLIENT_DECRYPT,
        GNUNET_MESSAGE_TYPE_SECRETSHARING_CLIENT_DECRYPT_DONE,
        GNUNET_MESSAGE_TYPE_SECRETSHARING_CLIENT_GENERATE,
        GNUNET_MESSAGE_TYPE_SECRETSHARING_CLIENT_SECRET_READY, GNUNET_MESSAGE_TYPE_SET_ACCEPT,
        GNUNET_MESSAGE_TYPE_SET_ADD, GNUNET_MESSAGE_TYPE_SET_CANCEL,
        GNUNET_MESSAGE_TYPE_SET_CONCLUDE, GNUNET_MESSAGE_TYPE_SET_COPY_LAZY_CONNECT,
        GNUNET_MESSAGE_TYPE_SET_COPY_LAZY_PREPARE, GNUNET_MESSAGE_TYPE_SET_COPY_LAZY_RESPONSE,
        GNUNET_MESSAGE_TYPE_SET_CREATE, GNUNET_MESSAGE_TYPE_SET_EVALUATE,
        GNUNET_MESSAGE_TYPE_SET_ITER_ACK, GNUNET_MESSAGE_TYPE_SET_ITER_DONE,
        GNUNET_MESSAGE_TYPE_SET_ITER_ELEMENT, GNUNET_MESSAGE_TYPE_SET_ITER_REQUEST,
        GNUNET_MESSAGE_TYPE_SET_LISTEN, GNUNET_MESSAGE_TYPE_SET_REJECT,
        GNUNET_MESSAGE_TYPE_SET_REMOVE, GNUNET_MESSAGE_TYPE_SET_REQUEST,
        GNUNET_MESSAGE_TYPE_SET_RESULT, GNUNET_MESSAGE_TYPE_SOCIAL_APP_CONNECT,
        GNUNET_MESSAGE_TYPE_SOCIAL_APP_DETACH, GNUNET_MESSAGE_TYPE_SOCIAL_APP_EGO,
        GNUNET_MESSAGE_TYPE_SOCIAL_APP_EGO_END, GNUNET_MESSAGE_TYPE_SOCIAL_APP_PLACE,
        GNUNET_MESSAGE_TYPE_SOCIAL_APP_PLACE_END, GNUNET_MESSAGE_TYPE_SOCIAL_ENTRY_DECISION,
        GNUNET_MESSAGE_TYPE_SOCIAL_ENTRY_REQUEST, GNUNET_MESSAGE_TYPE_SOCIAL_GUEST_ENTER,
        GNUNET_MESSAGE_TYPE_SOCIAL_GUEST_ENTER_ACK, GNUNET_MESSAGE_TYPE_SOCIAL_GUEST_ENTER_BY_NAME,
        GNUNET_MESSAGE_TYPE_SOCIAL_HOST_ENTER, GNUNET_MESSAGE_TYPE_SOCIAL_HOST_ENTER_ACK,
        GNUNET_MESSAGE_TYPE_SOCIAL_MSG_PROC_CLEAR, GNUNET_MESSAGE_TYPE_SOCIAL_MSG_PROC_SET,
        GNUNET_MESSAGE_TYPE_SOCIAL_PLACE_LEAVE, GNUNET_MESSAGE_TYPE_SOCIAL_PLACE_LEAVE_ACK,
        GNUNET_MESSAGE_TYPE_SOCIAL_RESULT_CODE, GNUNET_MESSAGE_TYPE_SOCIAL_ZONE_ADD_NYM,
        GNUNET_MESSAGE_TYPE_SOCIAL_ZONE_ADD_PLACE, GNUNET_MESSAGE_TYPE_STATISTICS_DISCONNECT,
        GNUNET_MESSAGE_TYPE_STATISTICS_DISCONNECT_CONFIRM, GNUNET_MESSAGE_TYPE_STATISTICS_END,
        GNUNET_MESSAGE_TYPE_STATISTICS_GET, GNUNET_MESSAGE_TYPE_STATISTICS_SET,
        GNUNET_MESSAGE_TYPE_STATISTICS_VALUE, GNUNET_MESSAGE_TYPE_STATISTICS_WATCH,
        GNUNET_MESSAGE_TYPE_STATISTICS_WATCH_VALUE, GNUNET_MESSAGE_TYPE_TEST,
        GNUNET_MESSAGE_TYPE_TESTBED_ADD_HOST, GNUNET_MESSAGE_TYPE_TESTBED_ADD_HOST_SUCCESS,
        GNUNET_MESSAGE_TYPE_TESTBED_BARRIER_CANCEL, GNUNET_MESSAGE_TYPE_TESTBED_BARRIER_INIT,
        GNUNET_MESSAGE_TYPE_TESTBED_BARRIER_STATUS, GNUNET_MESSAGE_TYPE_TESTBED_BARRIER_WAIT,
        GNUNET_MESSAGE_TYPE_TESTBED_CONFIGURE_UNDERLAY_LINK,
        GNUNET_MESSAGE_TYPE_TESTBED_CREATE_PEER, GNUNET_MESSAGE_TYPE_TESTBED_CREATE_PEER_SUCCESS,
        GNUNET_MESSAGE_TYPE_TESTBED_DESTROY_PEER,
        GNUNET_MESSAGE_TYPE_TESTBED_GENERIC_OPERATION_SUCCESS,
        GNUNET_MESSAGE_TYPE_TESTBED_GET_PEER_INFORMATION,
        GNUNET_MESSAGE_TYPE_TESTBED_GET_SLAVE_CONFIGURATION,
        GNUNET_MESSAGE_TYPE_TESTBED_HELPER_INIT, GNUNET_MESSAGE_TYPE_TESTBED_HELPER_REPLY,
        GNUNET_MESSAGE_TYPE_TESTBED_INIT, GNUNET_MESSAGE_TYPE_TESTBED_LINK_CONTROLLERS,
        GNUNET_MESSAGE_TYPE_TESTBED_LINK_CONTROLLERS_RESULT,
        GNUNET_MESSAGE_TYPE_TESTBED_LOGGER_ACK, GNUNET_MESSAGE_TYPE_TESTBED_LOGGER_MSG,
        GNUNET_MESSAGE_TYPE_TESTBED_MANAGE_PEER_SERVICE, GNUNET_MESSAGE_TYPE_TESTBED_MAX,
        GNUNET_MESSAGE_TYPE_TESTBED_OPERATION_FAIL_EVENT,
        GNUNET_MESSAGE_TYPE_TESTBED_OVERLAY_CONNECT,
        GNUNET_MESSAGE_TYPE_TESTBED_PEER_CONNECT_EVENT, GNUNET_MESSAGE_TYPE_TESTBED_PEER_EVENT,
        GNUNET_MESSAGE_TYPE_TESTBED_PEER_INFORMATION, GNUNET_MESSAGE_TYPE_TESTBED_RECONFIGURE_PEER,
        GNUNET_MESSAGE_TYPE_TESTBED_REMOTE_OVERLAY_CONNECT,
        GNUNET_MESSAGE_TYPE_TESTBED_SHUTDOWN_PEERS,
        GNUNET_MESSAGE_TYPE_TESTBED_SLAVE_CONFIGURATION, GNUNET_MESSAGE_TYPE_TESTBED_START_PEER,
        GNUNET_MESSAGE_TYPE_TESTBED_STOP_PEER,
        GNUNET_MESSAGE_TYPE_TRANSPORT_ADDRESS_CONSIDER_VERIFY,
        GNUNET_MESSAGE_TYPE_TRANSPORT_ADDRESS_TO_STRING,
        GNUNET_MESSAGE_TYPE_TRANSPORT_ADDRESS_TO_STRING_REPLY,
        GNUNET_MESSAGE_TYPE_TRANSPORT_ADD_ADDRESS, GNUNET_MESSAGE_TYPE_TRANSPORT_ATS,
        GNUNET_MESSAGE_TYPE_TRANSPORT_BACKCHANNEL_ENCAPSULATION,
        GNUNET_MESSAGE_TYPE_TRANSPORT_BLACKLIST_INIT,
        GNUNET_MESSAGE_TYPE_TRANSPORT_BLACKLIST_QUERY,
        GNUNET_MESSAGE_TYPE_TRANSPORT_BLACKLIST_REPLY,
        GNUNET_MESSAGE_TYPE_TRANSPORT_BROADCAST_BEACON,
        GNUNET_MESSAGE_TYPE_TRANSPORT_COMMUNICATOR_BACKCHANNEL,
        GNUNET_MESSAGE_TYPE_TRANSPORT_COMMUNICATOR_BACKCHANNEL_INCOMING,
        GNUNET_MESSAGE_TYPE_TRANSPORT_COMMUNICATOR_FC_LIMITS,
        GNUNET_MESSAGE_TYPE_TRANSPORT_COMMUNICATOR_KX_CONFIRMATION,
        GNUNET_MESSAGE_TYPE_TRANSPORT_CONNECT, GNUNET_MESSAGE_TYPE_TRANSPORT_DEL_ADDRESS,
        GNUNET_MESSAGE_TYPE_TRANSPORT_DISCONNECT, GNUNET_MESSAGE_TYPE_TRANSPORT_DV_BOX,
        GNUNET_MESSAGE_TYPE_TRANSPORT_DV_LEARN, GNUNET_MESSAGE_TYPE_TRANSPORT_FRAGMENT,
        GNUNET_MESSAGE_TYPE_TRANSPORT_FRAGMENT_ACK, GNUNET_MESSAGE_TYPE_TRANSPORT_INCOMING_MSG,
        GNUNET_MESSAGE_TYPE_TRANSPORT_INCOMING_MSG_ACK, GNUNET_MESSAGE_TYPE_TRANSPORT_MONITOR_DATA,
        GNUNET_MESSAGE_TYPE_TRANSPORT_MONITOR_END,
        GNUNET_MESSAGE_TYPE_TRANSPORT_MONITOR_PEER_REQUEST,
        GNUNET_MESSAGE_TYPE_TRANSPORT_MONITOR_PEER_RESPONSE,
        GNUNET_MESSAGE_TYPE_TRANSPORT_MONITOR_PEER_RESPONSE_END,
        GNUNET_MESSAGE_TYPE_TRANSPORT_MONITOR_PLUGIN_EVENT,
        GNUNET_MESSAGE_TYPE_TRANSPORT_MONITOR_PLUGIN_START,
        GNUNET_MESSAGE_TYPE_TRANSPORT_MONITOR_PLUGIN_SYNC,
        GNUNET_MESSAGE_TYPE_TRANSPORT_MONITOR_START,
        GNUNET_MESSAGE_TYPE_TRANSPORT_NEW_COMMUNICATOR, GNUNET_MESSAGE_TYPE_TRANSPORT_PING,
        GNUNET_MESSAGE_TYPE_TRANSPORT_PONG, GNUNET_MESSAGE_TYPE_TRANSPORT_QUEUE_CREATE,
        GNUNET_MESSAGE_TYPE_TRANSPORT_QUEUE_CREATE_FAIL,
        GNUNET_MESSAGE_TYPE_TRANSPORT_QUEUE_CREATE_OK, GNUNET_MESSAGE_TYPE_TRANSPORT_QUEUE_SETUP,
        GNUNET_MESSAGE_TYPE_TRANSPORT_QUEUE_TEARDOWN, GNUNET_MESSAGE_TYPE_TRANSPORT_RECV,
        GNUNET_MESSAGE_TYPE_TRANSPORT_RELIABILITY_ACK,
        GNUNET_MESSAGE_TYPE_TRANSPORT_RELIABILITY_BOX, GNUNET_MESSAGE_TYPE_TRANSPORT_SEND,
        GNUNET_MESSAGE_TYPE_TRANSPORT_SEND_MSG, GNUNET_MESSAGE_TYPE_TRANSPORT_SEND_MSG_ACK,
        GNUNET_MESSAGE_TYPE_TRANSPORT_SEND_OK, GNUNET_MESSAGE_TYPE_TRANSPORT_SESSION_ACK,
        GNUNET_MESSAGE_TYPE_TRANSPORT_SESSION_DISCONNECT,
        GNUNET_MESSAGE_TYPE_TRANSPORT_SESSION_KEEPALIVE,
        GNUNET_MESSAGE_TYPE_TRANSPORT_SESSION_KEEPALIVE_RESPONSE,
        GNUNET_MESSAGE_TYPE_TRANSPORT_SESSION_QUOTA, GNUNET_MESSAGE_TYPE_TRANSPORT_SESSION_SYN,
        GNUNET_MESSAGE_TYPE_TRANSPORT_SESSION_SYN_ACK, GNUNET_MESSAGE_TYPE_TRANSPORT_SET_QUOTA,
        GNUNET_MESSAGE_TYPE_TRANSPORT_START, GNUNET_MESSAGE_TYPE_TRANSPORT_SUGGEST,
        GNUNET_MESSAGE_TYPE_TRANSPORT_SUGGEST_CANCEL, GNUNET_MESSAGE_TYPE_TRANSPORT_TCP_NAT_PROBE,
        GNUNET_MESSAGE_TYPE_TRANSPORT_TCP_WELCOME, GNUNET_MESSAGE_TYPE_TRANSPORT_TRAFFIC_METRIC,
        GNUNET_MESSAGE_TYPE_TRANSPORT_UDP_ACK, GNUNET_MESSAGE_TYPE_TRANSPORT_UDP_MESSAGE,
        GNUNET_MESSAGE_TYPE_TRANSPORT_XU_MESSAGE, GNUNET_MESSAGE_TYPE_VPN_CLIENT_REDIRECT_TO_IP,
        GNUNET_MESSAGE_TYPE_VPN_CLIENT_REDIRECT_TO_SERVICE, GNUNET_MESSAGE_TYPE_VPN_CLIENT_USE_IP,
        GNUNET_MESSAGE_TYPE_VPN_DNS_FROM_INTERNET, GNUNET_MESSAGE_TYPE_VPN_DNS_TO_INTERNET,
        GNUNET_MESSAGE_TYPE_VPN_HELPER, GNUNET_MESSAGE_TYPE_VPN_ICMP_TO_INTERNET,
        GNUNET_MESSAGE_TYPE_VPN_ICMP_TO_SERVICE, GNUNET_MESSAGE_TYPE_VPN_ICMP_TO_VPN,
        GNUNET_MESSAGE_TYPE_VPN_TCP_DATA_TO_EXIT, GNUNET_MESSAGE_TYPE_VPN_TCP_DATA_TO_VPN,
        GNUNET_MESSAGE_TYPE_VPN_TCP_TO_INTERNET_START,
        GNUNET_MESSAGE_TYPE_VPN_TCP_TO_SERVICE_START, GNUNET_MESSAGE_TYPE_VPN_UDP_REPLY,
        GNUNET_MESSAGE_TYPE_VPN_UDP_TO_INTERNET, GNUNET_MESSAGE_TYPE_VPN_UDP_TO_SERVICE,
        GNUNET_MESSAGE_TYPE_WDHT_GET, GNUNET_MESSAGE_TYPE_WDHT_GET_RESULT,
        GNUNET_MESSAGE_TYPE_WDHT_PUT, GNUNET_MESSAGE_TYPE_WDHT_RANDOM_WALK,
        GNUNET_MESSAGE_TYPE_WDHT_RANDOM_WALK_RESPONSE, GNUNET_MESSAGE_TYPE_WDHT_SUCCESSOR_FIND,
        GNUNET_MESSAGE_TYPE_WDHT_TRAIL_DESTROY, GNUNET_MESSAGE_TYPE_WDHT_TRAIL_ROUTE,
        GNUNET_MESSAGE_TYPE_WLAN_ADVERTISEMENT, GNUNET_MESSAGE_TYPE_WLAN_DATA,
        GNUNET_MESSAGE_TYPE_WLAN_DATA_FROM_HELPER, GNUNET_MESSAGE_TYPE_WLAN_DATA_TO_HELPER,
        GNUNET_MESSAGE_TYPE_WLAN_HELPER_CONTROL, GNUNET_MQ_PREFERENCE_COUNT, GNUNET_NO,
        GNUNET_NT_COUNT, GNUNET_OK, GNUNET_SYSERR, GNUNET_TERM_SIG, GNUNET_TUN_DNS_CLASS_CHAOS,
        GNUNET_TUN_DNS_CLASS_HESIOD, GNUNET_TUN_DNS_CLASS_INTERNET,
        GNUNET_TUN_DNS_OPCODE_INVERSE_QUERY, GNUNET_TUN_DNS_OPCODE_QUERY,
        GNUNET_TUN_DNS_OPCODE_STATUS, GNUNET_TUN_DNS_RETURN_CODE_FORMAT_ERROR,
        GNUNET_TUN_DNS_RETURN_CODE_NAME_ERROR, GNUNET_TUN_DNS_RETURN_CODE_NOT_AUTH,
        GNUNET_TUN_DNS_RETURN_CODE_NOT_IMPLEMENTED, GNUNET_TUN_DNS_RETURN_CODE_NOT_ZONE,
        GNUNET_TUN_DNS_RETURN_CODE_NO_ERROR, GNUNET_TUN_DNS_RETURN_CODE_NXRRSET,
        GNUNET_TUN_DNS_RETURN_CODE_REFUSED, GNUNET_TUN_DNS_RETURN_CODE_SERVER_FAILURE,
        GNUNET_TUN_DNS_RETURN_CODE_YXDOMAIN, GNUNET_TUN_DNS_RETURN_CODE_YXRRSET,
        GNUNET_TUN_ICMPTYPE_DESTINATION_UNREACHABLE, GNUNET_TUN_ICMPTYPE_ECHO_REPLY,
        GNUNET_TUN_ICMPTYPE_ECHO_REQUEST, GNUNET_TUN_ICMPTYPE_REDIRECT_MESSAGE,
        GNUNET_TUN_ICMPTYPE_ROUTER_ADVERTISEMENT, GNUNET_TUN_ICMPTYPE_ROUTER_SOLICITATION,
        GNUNET_TUN_ICMPTYPE_SOURCE_QUENCH, GNUNET_TUN_ICMPTYPE_TIME_EXCEEDED,
        GNUNET_TUN_TCP_FLAGS_ACK, GNUNET_TUN_TCP_FLAGS_CWR, GNUNET_TUN_TCP_FLAGS_ECE,
        GNUNET_TUN_TCP_FLAGS_FIN, GNUNET_TUN_TCP_FLAGS_PSH, GNUNET_TUN_TCP_FLAGS_RST,
        GNUNET_TUN_TCP_FLAGS_SYN, GNUNET_TUN_TCP_FLAGS_URG, GNUNET_UTIL_VERSION, GNUNET_YES,
};
