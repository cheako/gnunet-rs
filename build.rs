extern crate bindgen;
extern crate pkg_config;

use std::collections::HashSet;

use std::env;
use std::path::PathBuf;

fn gnunet_config() -> pkg_config::Library {
        pkg_config::Config::new()
                .cargo_metadata(false)
                .arg("gnunetarm")
                .arg("gnunetidentity")
                .arg("gnunetnamestore")
                .arg("gnunetgns")
                .arg("gnunetdht")
                .probe("gnunetutil")
                .unwrap()
}

#[derive(Debug)]
struct IgnoreMacros(HashSet<String>);

impl bindgen::callbacks::ParseCallbacks for IgnoreMacros {
        fn will_parse_macro(&self, name: &str) -> bindgen::callbacks::MacroParsingBehavior {
                if self.0.contains(name) {
                        bindgen::callbacks::MacroParsingBehavior::Ignore
                } else {
                        bindgen::callbacks::MacroParsingBehavior::Default
                }
        }
}

fn main() {
        let ignored_macros = IgnoreMacros(
                vec![
                        "FP_INFINITE".into(),
                        "FP_NAN".into(),
                        "FP_NORMAL".into(),
                        "FP_SUBNORMAL".into(),
                        "FP_ZERO".into(),
                        "IPPORT_RESERVED".into(),
                ]
                .into_iter()
                .collect(),
        );

        println!("cargo:rerun-if-env-changed=GNUNET_DIR");
        let gnunet_dir = env::var("GNUNET_DIR").map(PathBuf::from);

        println!("cargo:rerun-if-env-changed=GNUNET_LIB_DIRS");
        let lib_dirs = env::var("GNUNET_LIB_DIRS")
                .map(|x| x.split(":").map(PathBuf::from).collect::<Vec<PathBuf>>())
                .or_else(|_| Ok(vec![gnunet_dir.clone()?.join("lib")]))
                .or_else(|_: env::VarError| -> Result<_, env::VarError> {
                        Ok(gnunet_config().link_paths)
                })
                .expect("Couldn't find Gnunet library directory");
        for d in &lib_dirs {
                if !d.exists() {
                        panic!(
                                "Gnunet library directory does not exist: {}",
                                d.to_string_lossy()
                        );
                }
                println!("cargo:rustc-link-search=native={}", d.to_string_lossy());
        }

        println!("cargo:rerun-if-env-changed=GNUNET_INCLUDE_DIRS");
        let include_dirs = env::var("GNUNET_INCLUDE_DIRS")
                .map(|x| x.split(":").map(PathBuf::from).collect::<Vec<PathBuf>>())
                .or_else(|_| Ok(vec![gnunet_dir.clone()?.join("include")]))
                .or_else(|_: env::VarError| -> Result<_, env::VarError> {
                        Ok(gnunet_config().include_paths)
                })
                .expect("Couldn't find Gnunet include directory");

        println!("cargo:rerun-if-env-changed=GNUNET_LIBS");
        let libs_env = env::var("GNUNET_LIBS").ok();
        let libs = match libs_env {
                Some(ref v) => v.split(":").map(|x| x.to_owned()).collect(),
                None => gnunet_config().libs,
        };

        let kind = determine_mode(&lib_dirs, libs.as_slice());
        for lib in libs.into_iter() {
                println!("cargo:rustc-link-lib={}={}", kind, lib);
        }

        for d in &include_dirs {
                if !d.exists() {
                        panic!(
                                "Gnunet include directory does not exist: {}",
                                d.to_string_lossy()
                        );
                }
                println!("cargo:include={}", d.to_string_lossy());
        }

        // The bindgen::Builder is the main entry point
        // to bindgen, and lets you build up options for
        // the resulting bindings.
        let bindings = bindgen::Builder::default()
                // The input header we would like to generate
                // bindings for.
                .header("wrapper.h")
                .parse_callbacks(Box::new(ignored_macros))
                // Finish the builder and generate the bindings.
                .generate()
                // Unwrap the Result and panic on failure.
                .expect("Unable to generate bindings");

        // Write the bindings to the $OUT_DIR/bindings.rs file.
        let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
        bindings.write_to_file(out_path.join("bindings.rs"))
                .expect("Couldn't write bindings!");
}

fn determine_mode<T: AsRef<str>>(libdirs: &Vec<PathBuf>, libs: &[T]) -> &'static str {
        println!("cargo:rerun-if-env-changed=GNUNET_STATIC");
        let kind = env::var("GNUNET_STATIC").ok();
        match kind.as_ref().map(|s| &s[..]) {
                Some("0") => return "dylib",
                Some(_) => return "static",
                None => {}
        }

        // See what files we actually have to link against, and see what our
        // possibilities even are.
        let files = libdirs
                .into_iter()
                .flat_map(|d| d.read_dir().unwrap())
                .map(|e| e.unwrap())
                .map(|e| e.file_name())
                .filter_map(|e| e.into_string().ok())
                .collect::<HashSet<_>>();
        let can_static = libs.iter().all(|l| {
                files.contains(&format!("lib{}.a", l.as_ref()))
                        || files.contains(&format!("{}.lib", l.as_ref()))
        });
        let can_dylib = libs.iter().all(|l| {
                files.contains(&format!("lib{}.so", l.as_ref()))
                        || files.contains(&format!("{}.dll", l.as_ref()))
                        || files.contains(&format!("lib{}.dylib", l.as_ref()))
        });

        match (can_static, can_dylib) {
                (true, false) => return "static",
                (false, true) => return "dylib",
                (false, false) => {
                        panic!(
                                "Gnunet libdirs at `{:?}` do not contain the required files \
                                 to either statically or dynamically link Gnunet",
                                libdirs
                        );
                }
                (true, true) => {}
        }

        // default
        "dylib"
}
